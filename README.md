# README #

Script to solve All Different Directions quiz.
Repository includes a few classes with methods to solve quiz named All Different Directions.
See instructions here https://open.kattis.com/problems/alldifferentdirections

### Usage ###

See `Usage` section in the end of the file.


### Contacts ###

[ Alexander Palamarchuk ](mailto:apalamar4uk@gmail.com)

[ LinkedIn ](https://www.linkedin.com/in/palamarchukalexander/)