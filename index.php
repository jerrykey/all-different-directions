<?php

/**
 * Script to solve All Different Directions quiz.
 *
 * File includes a few classes with methods to solve quiz named All Different Directions.
 * See instructions here https://open.kattis.com/problems/alldifferentdirections
 *
 * PHP version 7.0
 *
 * @category   Quiz
 * @package    alldifferentdirections
 * @author     Alexander Palamarchuk <apalamar4uk@gmail.com>
 * @copyright  2018
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    0.0.2
 */

/**
 * Coordinates Class
 *
 * Class describes the point with x and y coordinates. Default point is 0,0.
 * 
 * @param integer $x The x coordinate
 * @param integer $y The y coordinate
 */
class Coords {

    public $x;
    public $y;

    public function __construct($x = 0, $y = 0) {
        $this->x = $x;
        $this->y = $y;
    }

}

/**
 * One step movement
 *
 * Class describes the movement in unit distance by direction
 * 
 * @param integer $angle The direction to move. Angle direction in degrees (east is 0 degrees, north is 90 degrees).
 * @param integer $distance The distance to move in units
 */
class Move {

    public $angle;
    public $distance;

    public function __construct($angle = 0, $distance = 0) {
        $this->angle = $angle;
        $this->distance = $distance;
    }

}

/**
 * Class with methods to calculate coordinates, distances, averages and worst values.
 * 
 * Methods to solve All Different Directions quiz.
 */
class allDifferentDirections {

    protected $accurate;

    public function __construct() {
        $this->accurate = 4;
    }

    /**
     * Solve one case with person's instructions
     * 
     * @param Array $caseArray Lines with move instructions
     * @return Array Quiz results. Array contains the x and y of average coordinates and the worst distance
     */
    public function solveCase($caseArray) {

        $moveCoordinates = [];
        $moveDistances = [];

        //Calculate all new coordinates from person's instructions
        foreach ($caseArray as $line) {
            array_push($moveCoordinates, $this->calcMoveCoords($line));
        }

        //Calculate avarage coordinates
        $averageCoordinates = $this->calcAverage($moveCoordinates);

        //Calculate all distances
        foreach ($moveCoordinates as $coord) {
            array_push($moveDistances, $this->calcDistance($coord, $averageCoordinates));
        }

        //Calculate worst distance

        $worstDistance = $this->getWorstDistance($moveDistances);

        return [$averageCoordinates->x, $averageCoordinates->y, $worstDistance];
    }

    /**
     * Calculates new coordinates from one person's instruction
     *
     * @param string $line Person's instructions
     * @return Coords New coordinates after movement instructions
     */
    protected function calcMoveCoords($line) {
        $splitLine = explode(' ', $line);

        $stepCoords = new Coords($splitLine[0], $splitLine[1]);
        $stepAngle = 0;

        for ($i = 2; $i < count($splitLine); $i += 2) {

            $stepWalk = 0;

            switch ($splitLine[$i]) {
                case 'start':
                case 'turn':
                    $stepAngle = (float) $stepAngle + $splitLine[$i + 1];
                    break;
                case 'walk':
                    $stepWalk = (float) $stepWalk + $splitLine[$i + 1];
                    break;
            }

            $stepCoords = $this->calcCoords($stepCoords, new Move($stepAngle, $stepWalk));
        }

        return $stepCoords;
    }

    /**
     * This function calculates new coordinates after one step moving.
     * 
     * @param \Coords $coords Initial coordinates where you stay
     * @param \Move $move Movement direction and distance
     * @return \Coords
     */
    protected function calcCoords($coords, $move) {

        $newCoords = new Coords($coords->x, $coords->y);

        $newCoords->x += $move->distance * cos($move->angle * pi() / 180);
        $newCoords->y += $move->distance * sin($move->angle * pi() / 180);

        return $newCoords;
    }

    /**
     * Calculates distance between two coordinates
     *
     * @param Coords $from Coordinates of start point
     * @param Coords $to Coordinates of end point
     * @return integer Distance in units
     */
    protected function calcDistance($from, $to) {

        $a = abs($to->y) - abs($from->y);
        $b = abs($to->x) - abs($from->x);

        return round(sqrt(pow(abs($a), 2) + pow(abs($b), 2)), $this->accurate);
    }

    /**
     * Find coordinates of the average destination
     * 
     * @param Array $coords_array Array with various coordinates
     * @return \Coords
     */
    protected function calcAverage($coords_array) {
        $average = new Coords();

        $all_x = [];
        $all_y = [];

        foreach ($coords_array as $coord) {
            array_push($all_x, $coord->x);
            array_push($all_y, $coord->y);
        }

        $average->x = round(array_sum($all_x) / count($all_x), $this->accurate);
        $average->y = round(array_sum($all_y) / count($all_y), $this->accurate);

        return $average;
    }

    /**
     * Returns the worst distance value from the array of distances
     *
     * @param array $distances Array with distance values
     * @return integer Value of worst distance
     */
    protected function getWorstDistance($distances) {
        return max($distances);
    }

}

/**
 * Usage example
 */
$quiz = new allDifferentDirections();

// Read input file 
$input = file_get_contents('./sample.in');

//Convert text to array
$inputArr = preg_split("/((\r?\n)|(\r\n?))/", $input);

// Iterate over input array
for ($i = 0; $i < count($inputArr); $i++) {

    //Split line with string into array
    $splitLine = explode(' ', $inputArr[$i]);

    if (count($splitLine) == 1 && $splitLine != 0) {
        // Start case
        $singleCase = array_slice($inputArr, $i + 1, $inputArr[$i]);

        if (!empty($singleCase)) {
            $output = $quiz->solveCase($singleCase);

            //print results
            echo implode(' ', $output) . "\r\n";
        }
    } elseif (count($splitLine) == 1 && $splitLine == 0) {
        return;
    } else {
        // Skip lines
        continue;
    }
}
